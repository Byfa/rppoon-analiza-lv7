﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZAD5
{
    class RentVisitor : IVisitor
    {
        private const double rentPercentCost = 10.0;
        public double Visit(DVD DVDItem)
        {
            if (DVDItem.Type == DVDType.SOFTWARE)
            {
                return 0;
            }
            return DVDItem.Price * (rentPercentCost / 100);
        }
        public double Visit(VHS VHSItem)
        {
            return VHSItem.Price * (rentPercentCost / 100);
        }
        public double Visit(Book BookItem)
        {
            return BookItem.Price * (rentPercentCost / 100);
        }
    }
}
