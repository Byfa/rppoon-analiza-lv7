﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZAD5
{
    class Program
    {
        static void Main(string[] args)
        {
            BuyVisitor buyVisitor = new BuyVisitor();
            RentVisitor rentVisitor = new RentVisitor();

            DVD dvd = new DVD("Monty Python and The Holy Grail", DVDType.MOVIE, 39.99);
            DVD dvd2 = new DVD("Studio Clip Art", DVDType.SOFTWARE, 39.99);
            VHS vhs = new VHS("Digimon", 12.98);
            Book book = new Book("Art of War", 9.78);

            Console.WriteLine(dvd.ToString());
            Console.WriteLine(dvd2.ToString());
            Console.WriteLine(vhs.ToString());
            Console.WriteLine(book.ToString());

            Cart cart = new Cart();
            cart.Add(dvd);
            cart.Add(dvd2);
            cart.Add(vhs);
            cart.Add(book);

            Console.WriteLine("\nBuy bill: ");
            Console.WriteLine(dvd.ToString());
            Console.WriteLine("Rent price:" + dvd.Accept(rentVisitor));
            Console.WriteLine(dvd2.ToString());
            Console.WriteLine("Rent price:" + dvd2.Accept(rentVisitor));
            Console.WriteLine(vhs.ToString());
            Console.WriteLine("Rent price:" + vhs.Accept(rentVisitor));
            Console.WriteLine(book.ToString());
            Console.WriteLine("Rent price:" + book.Accept(rentVisitor));
            Console.WriteLine("\nTotal buy price: " + cart.Accept(buyVisitor));

            Console.WriteLine("\nRent bill: ");
            Console.WriteLine(dvd.ToString());
            Console.WriteLine("Rent price:" + dvd.Accept(rentVisitor));
            Console.WriteLine(dvd2.ToString());
            Console.WriteLine("Rent price:" + dvd2.Accept(rentVisitor));
            Console.WriteLine(vhs.ToString());
            Console.WriteLine("Rent price:" + vhs.Accept(rentVisitor));
            Console.WriteLine(book.ToString());
            Console.WriteLine("Rent price:" + book.Accept(rentVisitor));
            Console.WriteLine("\nTotal rent price: " + cart.Accept(rentVisitor));
            Console.WriteLine("\nTotal rent price for 14 days: " + cart.Accept(rentVisitor)*14);

            Console.ReadKey();
        }
    }
}
