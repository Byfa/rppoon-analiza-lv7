﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZAD1
{
    class BubbleSort : SortStrategy
    {
        public override void Sort(double[] array)
        {
            bool flag = true;
            for (int i = 0; (i <= array.Length - 1) && flag; i++)
            {
                flag = false;
                for (int j = 0; j < array.Length - 1; j++)
                {
                    if (array[j] > array[j + 1])
                    {
                        Swap(ref array[j], ref array[j + 1]);
                        flag = true;
                    }
                }
            }
        }
    }
}
