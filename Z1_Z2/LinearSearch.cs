﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZAD1
{
    class LinearSearch : SearchStrategy
    {
        public override int Search(double[] array, double number)
        {
            for (int i = 0; i < array.Length; i++)
            {
                if (number == array[i])
                {
                    return i;
                }
            }
            return -1;  //nema broja
        }
    }
}
