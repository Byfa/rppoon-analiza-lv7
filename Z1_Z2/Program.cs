﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZAD1
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] numberArray = new double[10];
            Random random = new Random();

            for (int i = 0; i < 10; i++)
            {
                numberArray[i] = 100.0 * random.NextDouble();
            }

            NumberSequence numberSequence = new NumberSequence(numberArray);
            Console.WriteLine("Number sequence without sort:\n" + numberSequence.ToString());

            numberSequence.SetSortStrategy(new BubbleSort());
            numberSequence.Sort();
            Console.WriteLine("Bubble sort:\n" + numberSequence.ToString());

            numberSequence = new NumberSequence(numberArray);
            numberSequence.SetSortStrategy(new CombSort());
            numberSequence.Sort();
            Console.WriteLine("Comb sort:\n" + numberSequence.ToString());

            numberSequence = new NumberSequence(numberArray);
            numberSequence.SetSortStrategy(new SequentialSort());
            numberSequence.Sort();
            Console.WriteLine("Sequential sort:\n" + numberSequence.ToString());

            
            //zad2 dodatak
            double numberKey = 19.06;
            numberSequence.SetSearchStrategy(new LinearSearch());
            Console.WriteLine("Linear search with key as: " + numberKey + ", result: "+ numberSequence.Search(numberKey));
            numberKey = numberSequence[6];
            Console.WriteLine("Linear search with key as: " + numberKey + ", result: " + numberSequence.Search(numberKey));

            Console.ReadKey();
        }
    }
}
