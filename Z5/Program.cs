﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZAD5
{
    class Program
    {
        static void Main(string[] args)
        {
            BuyVisitor buyVisitor = new BuyVisitor();

            DVD dvd = new DVD("Monty Python and The Holy Grail", DVDType.MOVIE, 39.99);
            VHS vhs = new VHS("Digimon", 12.98);
            Book book = new Book("Art of War", 9.78);

            Console.WriteLine(dvd.ToString());
            Console.WriteLine(vhs.ToString());
            Console.WriteLine(book.ToString());

            Console.WriteLine("\nBill: ");
            Console.WriteLine(dvd.ToString());
            Console.WriteLine("Price with taxes:" + dvd.Accept(buyVisitor));
            Console.WriteLine(vhs.ToString());
            Console.WriteLine("Price with taxes:" + vhs.Accept(buyVisitor));
            Console.WriteLine(book.ToString());
            Console.WriteLine("Price with taxes:" + book.Accept(buyVisitor));
            Console.WriteLine("\nTotal price:" + (dvd.Accept(buyVisitor) + vhs.Accept(buyVisitor) + book.Accept(buyVisitor)));

            Console.ReadKey();
        }
    }
}
